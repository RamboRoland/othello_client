class Rules:
    debug_status = False

    def __init__(self):
        return

    def setDebug(self, value):
        self.debug_status = value

    def debug(self, text):
        if(self.debug_status is True):
            print('DEBUG RULES: ' + text)

    def validateMoves(self, board, moves, player, opponent):
        validMoves = []

        for i in range(len(moves)):
            if(self.validateMove(board, moves[i], player, opponent)):
                validMoves.append(moves[i])

        return validMoves

    def validateMove(self, board, move, player, opponent):
        x = move[0]
        y = move[1]

        validMove = False

        # check diagonal upp left
        if not(validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x - i
                currentY = y - i

                if not(board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if(board.getSlot(currentX,currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if(opponentMove and board.getSlot(currentX,currentY) == player):
                    validMove = True
                    self.debug('upp left valid')
                    break

                self.debug('upp left not valid')
                break

        # check diagonal upp
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x
                currentY = y - i

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('upp valid')
                    break

                self.debug('upp not valid')
                break

        # check diagonal upp right
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x + i
                currentY = y - i

                if not (board.slotExists(currentX, currentY)):
                    self.debug('Slot does not exists')
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    self.debug('continue')
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    self.debug('upp right valid')
                    validMove = True
                    break

                self.debug('upp right not valid')
                break

        # check diagonal right
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x + i
                currentY = y

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('right valid')
                    break

                self.debug('right not valid')
                break

        # check diagonal down right
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x + i
                currentY = y + i

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('down right valid')
                    break

                self.debug('down right not valid')
                break

        # check diagonal down
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x
                currentY = y + i

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('down valid')
                    break

                self.debug('down not valid')
                break

        # check diagonal down left
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x - i
                currentY = y + i

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('down left valid')
                    break

                self.debug('down left not valid')
                break

        # check diagonal left
        if not (validMove):
            opponentMove = False
            for i in range(1, board.size):
                currentX = x - i
                currentY = y

                if not (board.slotExists(currentX, currentY)):
                    break

                # if oppenets slot
                if (board.getSlot(currentX, currentY) == opponent):
                    opponentMove = True
                    continue

                # if current players slot
                if (opponentMove and board.getSlot(currentX, currentY) == player):
                    validMove = True
                    self.debug('left valid')
                    break

                self.debug('left not valid')
                break

        return validMove

    def getOpponentEffectedSlots(self, board, move, player, opponent):
        x = move[0]
        y = move[1]
        slots = []

        # check upp left
        current_slots = []
        for i in range(1, board.size):
            currentX = x - i
            currentY = y - i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX+1, currentY+1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check upp
        current_slots = []
        for i in range(1, board.size):
            currentX = x
            currentY = y - i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX+1, currentY+1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check upp right
        current_slots = []
        for i in range(1, board.size):
            currentX = x + i
            currentY = y - i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check right
        current_slots = []
        for i in range(1, board.size):
            currentX = x + i
            currentY = y

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check down right
        current_slots = []
        for i in range(1, board.size):
            currentX = x + i
            currentY = y + i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check down
        current_slots = []
        for i in range(1, board.size):
            currentX = x
            currentY = y + i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check down left
        current_slots = []
        for i in range(1, board.size):
            currentX = x - i
            currentY = y + i

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        # check left
        current_slots = []
        for i in range(1, board.size):
            currentX = x - i
            currentY = y

            if not (board.slotExists(currentX, currentY)):
                break

            # if oppenets slot
            if (board.getSlot(currentX, currentY) is opponent):
                current_slots.append([currentX + 1, currentY + 1])
                continue

            # if current players slot
            if (board.getSlot(currentX, currentY) is player):
                slots += current_slots
                break

            break

        return slots