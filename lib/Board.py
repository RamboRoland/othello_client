from .Rules import Rules

class Board:
    size = 8
    board = []
    pieces = []
    rules = Rules()

    def __init__(self):
        # Create grid
        self.board = [[0 for y in range(self.size)] for x in range(self.size)]

        # Set default pieces
        halfSize = int(self.size/2)
        self.setBoardPiece(2, halfSize - 1, halfSize - 1)
        self.setBoardPiece(2, halfSize, halfSize)
        self.setBoardPiece(1, halfSize - 1, halfSize)
        self.setBoardPiece(1, halfSize, halfSize - 1)

    def updateBoard(self, boardData):
        row = 0
        col = 0
        for data in boardData:
            # Convert the byte string to integer.
            data = int.from_bytes(data, byteorder='big')

            # Find a prettier way to extract the data.
            first = (data & 0xC0) >> 6
            second = (data & 0x30) >> 4
            third = (data & 0x0C) >> 2
            fourth = (data & 0x03)

            # Find a better way to insert the data.
            self.board[row][col] = first
            self.board[row][col + 1] = second
            self.board[row][col + 2] = third
            self.board[row][col + 3] = fourth

            col += 4
            if col >= 8:
                col = 0
                row += 1

    def setBoardSize(self, size):
        self.size = size

    def getSlot(self, x ,y):
        if(self.slotExists(x,y)):
            # skit is inverted... just deal with it
            return self.board[y][x]

        return False

    def setBoardPiece(self, state, x, y):
        self.board[y][x] = state
        return

    def hasSlots(self):
        return self.getSlots() > 0

    def getSlots(self):
        slots = 0
        for y in range(self.size):
            for x in range(self.size):
                if(self.isSlotOpen(x,y)):
                    slots += 1

        return slots

    def getWhiteSlots(self):
        slots = 0
        for y in range(self.size):
            for x in range(self.size):
                if (self.isSlotWhite(x, y)):
                    slots += 1

        return slots

    def getBlackSlots(self):
        slots = 0
        for y in range(self.size):
            for x in range(self.size):
                if (self.isSlotBlack(x, y)):
                    slots += 1

        return slots

    def slotExists(self, x, y):
        if(x >= 0 and x < self.size and y >= 0 and y < self.size):
            return True

        return False

    def isSlotOpen(self, x, y):
        return self.getSlot(x, y) is 0

    def isSlotWhite(self, x, y):
        return self.getSlot(x, y) is 2

    def isSlotBlack(self, x, y):
        return self.getSlot(x, y) is 1

    def isSlotOccupied(self, x, y):
        return self.getSlot(x,y) in (1,2)

    def getMoves(self):
        moves = []

        for y in range(self.size):
            for x in range(self.size):
                if (self.isSlotOpen(x,y) and self.checkSurroundingTakenSlots(x,y)):
                    moves.append([x,y])

        return moves

    def getAllValidMoves(self):
        whiteMoves = self.getValidMoves(1, 2)
        blackMoves = self.getValidMoves(2, 1)
        return len(whiteMoves) > 0 or len(blackMoves) > 0

    def getValidMoves(self, player, opponent):
        return self.rules.validateMoves(self, self.getMoves(), player, opponent)

    def hasMoves(self, player, opponent):
        valid_moves = self.getValidMoves(player, opponent)
        return len(valid_moves) > 0

    def setMove(self, turn, player, opponent, move):
        self.setBoardPiece(turn, move[0], move[1])

        opponent_effected_slots = self.rules.getOpponentEffectedSlots(self, move, player, opponent)
        self.flipSlots(opponent_effected_slots)

    def checkSurroundingTakenSlots(self, x, y):
        surroundingMoves = [
            self.isSlotOccupied(x, y + 1),
            self.isSlotOccupied(x + 1, y),
            self.isSlotOccupied(x + 1, y + 1),
            self.isSlotOccupied(x + 1, y - 1),
            self.isSlotOccupied(x - 1, y + 1),
            self.isSlotOccupied(x - 1, y - 1),
            self.isSlotOccupied(x, y - 1),
            self.isSlotOccupied(x - 1, y)
        ]

        return any(surroundingMoves)

    def flipSlots(self, slots):
        for i in range(len(slots)):
            self.flipSlot(slots[i][0],slots[i][1])


    def flipSlot(self, x ,y):
        newSlotState = 1 if self.getSlot(x-1, y-1) is 2 else 2
        self.setBoardPiece(newSlotState, x-1, y-1)

    def render(self):
        for y in range(self.size):
            output = ''
            for x in range(self.size):
                output += self.printBoardSection(self.getSlot(x,y))

            print(output)

    def printBoardSection(self, identity):
        if (identity == 1):
            rowOutput = '\x1b[32m\u2588\x1b[0m '
        elif (identity == 2):
            rowOutput = '\x1b[37m\u2588\x1b[0m '
        else:
            rowOutput = '\x1b[35m\u2588\x1b[0m '

        return rowOutput