import sys
import argparse
import struct
import socket
import traceback
import random
import string

from .Board import Board
from .Player import Player
from .Rules import Rules

class Game:
    state = None
    board = None
    rules = Rules()
    players = []
    turn = None
    winner = None

    sock = None
    server_address = None
    buffer_size = 64



    def __init__(self, boardSize = 8):
        self.board = Board()
        self.board.setBoardSize(boardSize)
        self.players = []
        self.turn = 1 # Black always moves first
        self.winner = False

    def training(self):
        self.state = 'training'
        self.addBlackPlayer()
        self.addWhitePlayer()

    def bytes_to_int(bytes):
        return int.from_bytes(bytes, byteorder='big')

    def connect(self):
        self.state = 'online'
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.server_address = ('localhost', 1337)
        print('connecting to {} port {}'.format(*self.server_address))
        self.sock.connect(self.server_address)

        # Connect to server

    def start(self):
        data = self.sock.recv(1)
        player_id = self.bytes_to_int(data)
        print('Received the id', player_id)
        self.addPlayer(player_id)
        self.setTurn(player_id)

    def end(self):
        print('closing socket')
        self.sock.close()

    def sendMoves(self, x, y):
        print('Sending next move', x, y)
        move = 0
        move += x << 4
        move += y
        self.sock.send(struct.pack('c', bytes([move])))
        return self.sock.recv(1)

    def on(self):
        if not (self.board.getAllValidMoves()):
            # Evaluate a winner
            countBlack = self.board.getBlackSlots()
            countWhite = self.board.getWhiteSlots()

            if (countWhite > countBlack):
                self.winner = self.getPlayerWhiteId()
            elif (countBlack > countWhite):
                self.winner = self.getPlayerBlackId()
            else:
                self.winner = 3

            return False


        return True

    def addBlackPlayer(self):
        self.addPlayer(self.getPlayerBlackId())

    def addWhitePlayer(self):
        self.addPlayer(self.getPlayerWhiteId())

    def getPlayerWhiteId(self):
        return 2

    def getPlayerBlackId(self):
        return 1

    def addPlayer(self, identity):
        self.players.append(Player(identity))

    def getTurn(self):
        return self.turn

    def setTurn(self, player_id):
        self.turn = player_id

    def getCurrentPlayer(self):
        return self.turn

    def getCurrentOpponent(self):
        return 2 if self.turn == 1 else 1

    def nextTurn(self):
        self.turn = 2 if self.turn == 1 else 1