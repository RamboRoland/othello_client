import sys
import argparse
import struct
import socket
import traceback
import random
import string

from lib.Game import Game
from random import randint
from time import sleep

game = Game()
game.connect()

try:
    game.start()

    while(game.on()):
        data = game.sock.recv(game.buffer_size)
        unpacked = struct.unpack('17c', data)
        game.board.updateBoard(unpacked[1:])
        gameState = game.bytes_to_int(unpacked[:1][0])
        game.board.render()

        # Interpret game state
        if gameState & 8 == 8:
            print('Game is active!')
        if gameState & 16 == 16:
            print('No move, skipping turn')
            continue

        my_turn = True
        while my_turn:
            moves = game.board.getValidMoves(game.getCurrentPlayer(), game.getCurrentOpponent())
            rand_move = randint(0, len(moves) - 1)
            # make one of the moves
            # print('RANDOM MOVE: ', rand_move)

            positionX = moves[rand_move][0]
            positionY = moves[rand_move][1]

            data = game.sendMoves(positionX, positionY)
            if game.bytes_to_int(data) == 1:

                print('good move')
                my_turn = False
            else:
                print('bad move, try another move')
                print(game.board.board)
                print('move' + positionX + ', ' + positionY)
                game.board.render()
                sys.exit()

except Exception as e:
    print(traceback.format_exc())
finally:
    game.end()