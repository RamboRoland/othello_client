from lib.Game import Game
from random import randint
from time import sleep

for i in range(1, 10):
    game = Game()
    # game.rules.setDebug(True)

    game.board.render()

    while (game.on()):
        # what moves can i make
        moves = game.board.getValidMoves(game.getCurrentPlayer(), game.getCurrentOpponent())
        print(moves)

        if (len(moves) == 0):
            print('skip turn')
            game.nextTurn()
            continue

        rand_move = randint(0, len(moves) - 1)
        # make one of the moves
        # print('RANDOM MOVE: ', rand_move)

        positionX = moves[rand_move][0]
        positionY = moves[rand_move][1]

        print(positionX, positionY)

        # positionX = input("Please enter x position: ")
        # positionY = input("Please enter y position: ")

        game.board.setMove(game.getTurn(), game.getCurrentPlayer(), game.getCurrentOpponent(), [positionX, positionY])

        game.nextTurn()
        game.board.render()

        # sleep(0.1)

    if (game.winner is 1):
        print('WINNER: Black')

    if (game.winner is 2):
        print('WINNER: White')

    if (game.winner is 3):
        print('WINNER: No one!')
